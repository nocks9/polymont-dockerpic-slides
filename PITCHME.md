# PIC Gitlab/Docker

Evolution de la solution d'intégration continue Polymont

De Jenkins vers Gitlab-CI

---

## Etude de l'existant et solutions apportées 

+++

### Des environnements Jenkins difficiles à maintenir à jour

La solution :
- Paravirtualisation Docker pour tous les composants
- Reverse proxy intégré Traefik
- Forge et CI avec Gitlab
- Monitoring avec Grafana et InfluxDB
- Peut utiliser en natif des infrastructures cloud (Kubernetes)

+++

### Pas d'environnement permettant les tests sur les projets MACIF

La solution :
- Registry Docker privée
- Environnements de test Mainframe émulés (Adabas/Natural)

+++

### Des outils qui ne sont plus en phase avec les nouvelles méthodes de travail

La solution :
- Intégration exhaustive du framework SCRUM

+++

### Une IHM peu ergonomique

La solution :
- Interface épurée, visualisation riche
- Messagerie instantanée avec Mattermost

---

### Des mises à jour simplifiées

- Un upgrade complet de la solution Gitlab-CI prend quelques minutes seulement
- Idem pour le retour AR si besoin
- Sont mis à jour le système, les librairies et les applications en une opération (containers)

+++

### Des environnements Mainframe de test à disposition

- Des environnemnts ADABAS/NATURAL éphémères de test peuvent maintenant être créés au besoin.
- Ils font partie de la chaîne d'intégration automatique.

+++

### Une solution Agile

- Un outillage orienté DevOps, fait pour des cycles de développement courts

+++

### Simplicité et efficacité

- L'administration de la solution est très simple à effectuer

---

### PIC

+++

CI : Intégration continue

+++

![Intégration continue](http://blog.soat.fr/wp-content/uploads/2010/03/Integration-continue.jpg)

+++

CD : Déploiement continu

+++

![Déploiement continu](https://blog.itil.org/wp-content/uploads/2016/07/DevOps-Cycle.png)

---

### Docker

- Packaging standardisé pour les applications et leurs dépendances
- Isolation des composants
- Partage du kernel
- Fonctionne avec la plupart des systèmes Linux et Windows

+++

### Docker : Layer-FS

+++

![Layerfs](https://coolshell.cn/wp-content/uploads/2015/08/docker-filesystems-busyboxrw.png)

+++

### Docker : (Para)virtualisation ?

Sous Windows également avec Hyper-V !

+++

![Docker-VS-VM](https://static.imasters.com.br/wp-content/uploads/2017/12/yury-01.png)

+++

### Les gains

- Vitesse : Pas d'OS à démarrer, application disponible en quelques secondes/minutes
- Portabilité : Moins de dépendances liées aux couches basses, possibilité de changer d'infrastructure
- Sécurité : Isolation des composants applicatifs, en natif

+++

### Pré-requis techniques

Overlay2 - Kernel

- Version 4.0 ou plus
- Version 3.10.0-514 ou plus pour RHEL ou CentOS

+++

Overlay2 - Filesystem

- ext4 (RHEL 7.1 uniquement)
- xfs (RHEL 7.2+ ), mais uniquement avec l'option d_type=true activée.

---

### Traefik

+++

![Traefik](https://d33wubrfki0l68.cloudfront.net/7c5fd7d38c371e23cdff059e6cebb10292cd441c/7d420/assets/img/traefik-architecture.svg)

---

### Gitlab CI

+++

![Gitlab-CI](http://codethebuild.github.io/slides/images/gitlab-ci-overview-small.png)

+++

### Gitlab-ci.yml

- Facile à lire, épuré
- Toutes les informations du CI au même endroit
- Le développeur peut consulter les jobs du CI
- Le développeur peut lancer ses propres jobs (DevOps)

+++

### Exemple

+++

```yaml
image: registry.gitlab.com/<USERNAME>/laravel-sample:latest

services:
  - mysql:5.7

variables:
  MYSQL_DATABASE: homestead
  MYSQL_ROOT_PASSWORD: secret
  DB_HOST: mysql
  DB_USERNAME: root

stages:
  - test
  - deploy

unit_test:
  stage: test
  script:
    - cp .env.example .env
    - composer install
    - php artisan key:generate
    - php artisan migrate
    - vendor/bin/phpunit

deploy_production:
  stage: deploy
  script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - ~/.composer/vendor/bin/envoy run deploy
  environment:
    name: production
    url: http://192.168.1.1
  when: manual
  only:
    - master
```

@[1](Image to work with)
@[3-4](Service to use)
@[6-10](Global Variables)
@[12-14](Stages)
@[16-23](Instructions for test stage)
@[25-39](Instructions for production stage)

+++

### Visualisation d'un pipeline

+++

![Sample](https://docs.gitlab.com/ee/ci/img/environments_manual_action_single_pipeline.png)

---

### Grafana

+++

![Grafana](http://i.imgur.com/gvP4v5M.png)

---

### Mattermost

+++

![Mattermost](https://www.supinfo.com/articles/resources/217348/2035/1.png)

---

### Avantages de la solution, comparée à Jenkins

- Très rapide à configurer
- Les instructions de CI sont stockées avec le code
- Mise en place des runners simplifiée
- Possibilité de partager un runner
- Keystore intégré pour les mots de passe, tokens et autres secrets
- Gestion simplifiée des artefacts
- Maintenance simplifiée (pas de plugin, mais une API REST HTTP)

+++

### Tableau comparatif des features

https://about.gitlab.com/comparison/gitlab-vs-jenkins.html

